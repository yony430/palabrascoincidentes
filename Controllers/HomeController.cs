﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using PalabrasCoincidentes.Models;
using System.IO;

namespace PalabrasCoincidentes.Controllers
{
    public class HomeController : Controller
    {
        //Retorna la vista principal de la solución web
        public IActionResult Index()
        {
            ViewBag.error = "";
            return View();
        }

        //Este metodo solo funciona con un llamado POST, recibe los archivos de registrados y contenido y procesa la información para definir quien es cliente
        //Da como resultado la misma vista principal pero con la confirmación de la ruta donde se genera el archivo RESULTADO
        [HttpPost("Home")]
        public IActionResult PalabrasCoincidentes(IFormFile registradostxt, IFormFile contenidotxt)
        {
            try
            {
                //Se inicializan las variables del proceso principal
                string error = "";
                string linea_registrado;
                Array array_contenido = ContenidoToArray(contenidotxt); //Mediante este metodo creo un array con cada una de las letras del archivo de contenido
                List<string> lista_clientes = new List<string>();

                var lector_registrados = new StreamReader(registradostxt.OpenReadStream()); //Declaro el lector para acceder a la información del archivo .txt

                //Se verifica el tamaño de los archivos para crear las excepciones de tamaño
                long tamanio_contenido = contenidotxt.Length;
                long tamanio_registrados = registradostxt.Length;

                if (tamanio_contenido == 0)
                {
                    error = "El archivo de contenido se encuentra vacio.";
                }
                else if (tamanio_registrados == 0)
                {
                    error = "El archivo de registrados se encuentra vacio.";
                }

                while ((linea_registrado = lector_registrados.ReadLine()) != null) //Recorro cada una de las lineas del archivo de registrados
                {

                    int longitud_registrado = linea_registrado.Length;
                    if (longitud_registrado > 0)
                    {
                        Boolean esCliente = true;
                        Array array_contenido_registro = (string[])array_contenido.Clone(); //Se crea un clon del arreglo de contenido para ir reemplazando las posiciones y asi la LETRA se utilice solo una vez
                        for (int i = 0; i < longitud_registrado; i++)
                        {
                            //Por cada una de las letras que conforman al cliente verifico que si exista en el array de contenido 
                            int posicion_encontrada = Array.IndexOf(array_contenido_registro, linea_registrado[i].ToString().ToUpper());
                            if (posicion_encontrada == -1)
                            {
                                esCliente = false;
                            }
                            else
                            {
                                array_contenido_registro.SetValue("", posicion_encontrada); //Una vez encuentre la letra se reemplaza esta posición con un ""
                            }
                        }

                        if (esCliente) //Es un cliente y lo adiciono al contenido del archivo RESULTADOS
                        {
                            lista_clientes.Add(linea_registrado);
                        }
                    }
                }

                if (error == "")
                { //Ha este momento no se tiene error entonces creo el archivo RESULTADO.txt
                    if (!CrearTxt(lista_clientes))
                    {
                        error = "No se pudo crear el archivo RESULTADO.txt";
                    }
                }


                ViewBag.error = error;
                ViewBag.path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/RESULTADO.txt"; //Envio a manera de información para el usuario la ruta en la que qeudo el archivo ESCRITORIO
                return View("Index");

            }
            catch
            {
                ViewBag.error = "Se presento un error en el proceso principal. Comuniquese con el administrador del sistema.";
                return View("Index");
            }

        }

        //Este metodo se encarga de crear el archivo RESULTADO.txt, resive la lista de clientes y los adiciona. El archivo es creado en el escritorio
        private Boolean CrearTxt(List<string> lista_clientes)
        {

            try {
                if (System.IO.File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/RESULTADO.txt"))
                {
                    System.IO.File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/RESULTADO.txt");
                }

                StreamWriter archivo_resultado = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/RESULTADO.txt", true);
                foreach (var cliente in lista_clientes)
                {
                    archivo_resultado.WriteLine(cliente);
                }
                archivo_resultado.Close();

                return true;

            }
            catch {
                return false;
            }

        }

        //Esta función devuelve un arreglo con la letra del contenido.txt en cada una de sus posiciones
        private Array ContenidoToArray(IFormFile contenidotxt)
        {
            string[] array_cotenido = new string[0];
            try {
                List<string> lista_contenido = new List<string>();
                string linea_contenido;

                var reader = new StreamReader(contenidotxt.OpenReadStream());
                while ((linea_contenido = reader.ReadLine()) != null)
                {
                    lista_contenido.Add(linea_contenido);
                }

                array_cotenido = lista_contenido.ToArray();
                return array_cotenido;

            }
            catch {
                return array_cotenido;
            }
            
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
